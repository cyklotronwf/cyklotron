package pl.edu.pw.fizyka.pojava.a3;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;


public class LeftPanel extends JPanel {

	/**
	 * 
	 */
	private static final long       serialVersionUID        = 1L;
	private GroupLayout layout;
	private MainPanel mainPanel;
	private ControlPanel controlPanel;
	private SettingsPanel settingsPanel;
	private Controller controller;
	private ResourceBundle bundle;
	private final static int ELECTRON = 0, PROTON = 1, DEUTER = 2, OTHER =3;

	public LeftPanel(MainPanel mp) {
		mainPanel = mp;
		bundle = mainPanel.getBundle();
		controller = mainPanel.getController();

		layout = new GroupLayout(this);
		this.setLayout(layout);

		this.add(settingsPanel = new SettingsPanel());
		settingsPanel.setPreferredSize(new Dimension(200,600));
		this.add(controlPanel = new ControlPanel());
		controlPanel.setPreferredSize(new Dimension(200,600));

		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);

		layout.setHorizontalGroup(layout.createParallelGroup()
				.addComponent(settingsPanel)
				.addComponent(controlPanel)
		);

		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(settingsPanel)
				.addComponent(controlPanel)
		);      

	}

	/*
	 * SettingsPanel:
	 * Subpanel     used for physical settings (particle, fields etc.)
	 */

	public class SettingsPanel extends JPanel {
		/*
		 * 
		 */
		private static final long       serialVersionUID        = 1L;

		GroupLayout layoutSettings, layoutField, layoutParticle;

		JPanel particlePanel, fieldPanel;

		JLabel massUnitLabel;
		JLabel  elementarChargeLabel, teslaLabel, kiloVoltLabel, megaHertzLabel;
		JLabel magneticLabel, voltageLabel, frequencyLabel;
		JLabel electronLabel, protonLabel, deuterLabel;
		JButton optimalButton;
		JRadioButton electronRadioButton, protonRadioButton, deuterRadioButton, otherRadioButton;
		JTextField otherMassField, otherChargeField, magneticField, voltageField, frequencyField;
		ButtonGroup particleButtonGroup;

		public SettingsPanel() {
			layoutSettings = new GroupLayout(this);
			this.setLayout(layoutSettings);

			createComponents();
			setNewLayout();
			addListeners();
		}

		private void createComponents() {
			particlePanel = new JPanel();
			fieldPanel = new JPanel();
			Dimension maxTextFieldSize = new Dimension(80,100);

			particlePanel.add(electronRadioButton = new JRadioButton());
			particlePanel.add(electronLabel = new JLabel(bundle.getString("electron") ));
			particlePanel.add(protonRadioButton = new JRadioButton());
			particlePanel.add(protonLabel = new JLabel(bundle.getString("proton")));
			particlePanel.add(deuterLabel = new JLabel(bundle.getString("deuter")));
			particlePanel.add(deuterRadioButton = new JRadioButton());
			particlePanel.add(otherRadioButton = new JRadioButton());
			particlePanel.add(otherMassField = new JTextField(8));
			otherMassField.setMaximumSize(maxTextFieldSize);
			particlePanel.add(massUnitLabel = new JLabel(bundle.getString("massUnit")));
			particlePanel.add(otherChargeField = new JTextField(8));
			otherChargeField.setMaximumSize(maxTextFieldSize);
			particlePanel.add(elementarChargeLabel = new JLabel(bundle.getString("eCharge")));

			fieldPanel.add(magneticLabel = new JLabel(bundle.getString("Bfield")));
			fieldPanel.add(magneticField = new JTextField(8));
			magneticField.setMaximumSize(maxTextFieldSize);
			fieldPanel.add(teslaLabel = new JLabel(bundle.getString("tesla")));
			fieldPanel.add(voltageLabel = new JLabel(bundle.getString("voltage")));
			fieldPanel.add(voltageField = new JTextField(8));
			voltageField.setMaximumSize(maxTextFieldSize);
			fieldPanel.add(kiloVoltLabel = new JLabel(bundle.getString("kiloVolt")));
			fieldPanel.add(frequencyLabel = new JLabel(bundle.getString("freq")));
			fieldPanel.add(frequencyField = new JTextField(8));
			frequencyField.setMaximumSize(maxTextFieldSize);
			fieldPanel.add(megaHertzLabel = new JLabel(bundle.getString("megaHerz")));

			this.add(optimalButton = new JButton(bundle.getString("optimal")));
			optimalButton.setMaximumSize(new Dimension(80,50));

			particleButtonGroup = new ButtonGroup();
			particleButtonGroup.add(electronRadioButton);
			particleButtonGroup.add(protonRadioButton);
			particleButtonGroup.add(deuterRadioButton);
			particleButtonGroup.add(otherRadioButton);
			electronRadioButton.setSelected(true);

			fieldPanel.setBorder(BorderFactory.createTitledBorder(
					bundle.getString("EMfield")));
			particlePanel.setBorder(BorderFactory.createTitledBorder(
					bundle.getString("part")));

			particlePanel.setPreferredSize(fieldPanel.getSize());
		}

		private void setNewLayout() {

			layoutSettings.setAutoCreateGaps(true);

			int interGap = 10;

			layoutSettings.setHorizontalGroup(layoutSettings
					.createParallelGroup(Alignment.CENTER)
					.addComponent(particlePanel)
					.addComponent(fieldPanel)
					.addComponent(optimalButton)
			);

			layoutSettings.setVerticalGroup(layoutSettings.createSequentialGroup()
					.addComponent(particlePanel).addGap(interGap)
					.addComponent(fieldPanel).addGap(interGap)
					.addComponent(optimalButton).addGap(interGap)
			);

			layoutParticle = new GroupLayout(particlePanel);
			particlePanel.setLayout(layoutParticle);
			layoutParticle.setAutoCreateContainerGaps(true);
			layoutParticle.setAutoCreateGaps(true);

			layoutParticle.setHorizontalGroup(layoutParticle.createSequentialGroup()
					.addGap(16)
					.addGroup(layoutParticle.
							createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(electronRadioButton)
							.addComponent(protonRadioButton)
							.addComponent(deuterRadioButton)
							.addComponent(otherRadioButton)
					)
					.addGroup(layoutParticle
							.createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(electronLabel)
							.addComponent(protonLabel)
							.addComponent(deuterLabel)
							.addGroup(layoutParticle.createSequentialGroup()
									.addComponent(otherMassField)
									.addComponent(massUnitLabel)
							)
							.addGroup(layoutParticle.createSequentialGroup()
									.addComponent(otherChargeField)
									.addComponent(elementarChargeLabel)
							)
					)
					.addGap(16)
			);

			layoutParticle.setVerticalGroup(layoutParticle.createSequentialGroup()
					.addGroup(layoutParticle.createParallelGroup(Alignment.BASELINE)
							.addComponent(electronRadioButton).addComponent(electronLabel)
					)
					.addGroup(layoutParticle.createParallelGroup(Alignment.BASELINE)
							.addComponent(protonRadioButton).addComponent(protonLabel)
					)
					.addGroup(layoutParticle.createParallelGroup(Alignment.BASELINE)
							.addComponent(deuterRadioButton).addComponent(deuterLabel)
					)
					.addGroup(layoutParticle.createParallelGroup(Alignment.BASELINE)
							.addComponent(otherRadioButton,Alignment.CENTER)
							.addGroup(layoutParticle.createSequentialGroup()
									.addGroup(layoutParticle.createParallelGroup(Alignment.BASELINE)
											.addComponent(otherMassField).addComponent(massUnitLabel))
											.addGroup(layoutParticle
													.createParallelGroup(Alignment.BASELINE)
													.addComponent(otherChargeField)
													.addComponent(elementarChargeLabel))
							)
					)
			);


			layoutField = new GroupLayout(fieldPanel);
			fieldPanel.setLayout(layoutField);
			layoutField.setAutoCreateContainerGaps(true);
			layoutField.setAutoCreateGaps(true);

			layoutField.setHorizontalGroup(layoutField.createSequentialGroup()
					.addGroup(layoutField.createParallelGroup(GroupLayout.Alignment.LEADING) 
							.addComponent(magneticLabel)
							.addComponent(voltageLabel)
							.addComponent(frequencyLabel)
					)
					.addGroup(layoutField.createParallelGroup(GroupLayout.Alignment.LEADING) 
							.addComponent(magneticField)
							.addComponent(voltageField)
							.addComponent(frequencyField)
					)
					.addGroup(layoutField.createParallelGroup(GroupLayout.Alignment.LEADING) 
							.addComponent(teslaLabel)
							.addComponent(kiloVoltLabel)
							.addComponent(megaHertzLabel)
					)
			);

			layoutField.setVerticalGroup(layoutField.createSequentialGroup()
					.addGroup(layoutField.createParallelGroup(Alignment.BASELINE)
							.addComponent(magneticLabel)
							.addGroup(layoutField.createSequentialGroup()
									.addGroup(layoutField.createParallelGroup(Alignment.BASELINE)
											.addComponent(magneticField).addComponent(teslaLabel))
							)
					)
					.addGroup(layoutField.createParallelGroup(Alignment.BASELINE)
							.addComponent(voltageLabel)
							.addGroup(layoutField.createSequentialGroup()
									.addGroup(layoutField.createParallelGroup(Alignment.BASELINE)
											.addComponent(voltageField).addComponent(kiloVoltLabel))
							)
					)
					.addGroup(layoutField.createParallelGroup(Alignment.BASELINE)
							.addComponent(frequencyLabel)
							.addGroup(layoutField.createSequentialGroup()
									.addGroup(layoutField.createParallelGroup(Alignment.BASELINE)
											.addComponent(frequencyField).addComponent(megaHertzLabel))
							)
					)
			);
		}

		private void addListeners() {
			optimalButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					setOptimalValues();
				}
			});
		}

		/*
		 * Blocks settings, called when animation is starting.
		 */
		private void blockSettings(boolean b) {
			electronRadioButton.setEnabled(!b);
			protonRadioButton.setEnabled(!b);
			deuterRadioButton.setEnabled(!b);
			otherRadioButton.setEnabled(!b);
			otherMassField.setEnabled(!b);
			otherChargeField.setEnabled(!b);
			magneticField.setEnabled(!b);
			voltageField.setEnabled(!b);
			frequencyField.setEnabled(!b);
			optimalButton.setEnabled(!b);
		}

		/*
		 * setOptimalValues:
		 * This method sets optimal values, based on following criteria:
		 * 1) B field for optimal turning force and turns count,
		 * 2) voltage for optimal acceleration during one turn,
		 * 3) frequency to set resonance.
		 */
		public void setOptimalValues() {
			double mass=getSelectedMass(), charge=getSelectedCharge();

			double B;
			int selectedParticle = getSelectedParticle();
			if (selectedParticle==ELECTRON) {
				B=0.1;
			} else if (selectedParticle==PROTON || selectedParticle==DEUTER) {
				B=1.2;
			} else {
				B=2*Math.PI*mass/(charge*1e-6);
			}

			double U = Math.abs(Math.pow(0.1*B,2)*charge/(4*mass));
			double f = Math.abs(B*charge/(2*mass*Math.PI));
			magneticField.setText(mainPanel.getNumberFormat().format(B));
			voltageField.setText(mainPanel.getNumberFormat().format(U/1e3));
			frequencyField.setText(mainPanel.getNumberFormat().format(f/1e6));  

		}

		// Returns selected particle type
		protected int getSelectedParticle() {
			if (electronRadioButton.isSelected()) {
				return ELECTRON;
			}
			else if (protonRadioButton.isSelected()) {
				return PROTON;
			}
			else if (deuterRadioButton.isSelected()) {
				return DEUTER;
			}
			else if (otherRadioButton.isSelected()){
				return OTHER;
			}
			else return ERROR;
		}

		// Return selected mass
		protected double getSelectedMass() throws NumberFormatException {
			switch(getSelectedParticle()) {
				case ELECTRON: return Physical.ELECTRON_MASS;
				case PROTON: return Physical.MASS_UNIT;
				case DEUTER: return Physical.MASS_UNIT*2;
				case OTHER: return Double.parseDouble(
						settingsPanel.otherMassField.getText())*
						Physical.MASS_UNIT;
				default: throw new NumberFormatException();
			}
		}

		// Returns selected charge
		protected double getSelectedCharge()  throws NumberFormatException {
			switch(getSelectedParticle()) {
				case ELECTRON: return -Physical.ELEMENTAR_CHARGE;
				case PROTON: return Physical.ELEMENTAR_CHARGE;
				case DEUTER: return Physical.ELEMENTAR_CHARGE;
				case OTHER: return Double.parseDouble(
						settingsPanel.otherChargeField.getText())*
						Physical.ELEMENTAR_CHARGE;
				default: throw new NumberFormatException();
			}
		}

		// Returns B field strength
		protected double getSelectedMagneticField()  throws NumberFormatException{
			return Double.parseDouble(settingsPanel.magneticField.getText());
		}

		// Returns chosen frequency
		protected double getSelectedFrequency()  throws NumberFormatException{
			return Double.parseDouble(settingsPanel.frequencyField.getText())*1e6;
		}

		// Returns chosen voltage
		protected double getSelectedVoltage()  throws NumberFormatException{
			return Double.parseDouble(settingsPanel.voltageField.getText())*1e3;
		}

		/*
		 * Creates Particle class based on typed values.
		 * Throws NumberformatException when any of above functions returns it,
		 * i.e. when data typed in fields is invalid (for example letters in number field).
		 */
		private Particle createParticleClass() throws NumberFormatException {
			return new Particle(getSelectedMass(),getSelectedCharge(),
					getSelectedMagneticField(), getSelectedVoltage(),
					getSelectedFrequency());
		}

	}


	/*
	 * ControlPanel:
	 * Subpanel used for controlling animation process.
	 */
	public class ControlPanel extends JPanel {
		/**
		 * 
		 */
		private static final long       serialVersionUID        = 1L;
		JButton startButton, pauseButton, stopButton, slowerButton, fasterButton;
		JLabel currentSpeedLabel;
		GroupLayout layout;

		JCheckBox showForceBox;

		public ControlPanel() {
			layout = new GroupLayout(this);
			this.setLayout(layout);

			this.setBorder(BorderFactory.createTitledBorder(bundle.getString("control")));

			createComponents();
			setNewLayout();
			addListeners();

		}

		private void createComponents() {
			Dimension defaultButtonSize = new Dimension(100,30);
			this.add(startButton = new JButton(bundle.getString("startBut")));
			this.add(pauseButton = new JButton(bundle.getString("pauseBut")));
			this.add(stopButton = new JButton(bundle.getString("stopBut")));
			this.add(fasterButton = new JButton(bundle.getString("fasterBut")));
			this.add(slowerButton = new JButton(bundle.getString("slowerBut")));
			this.add(currentSpeedLabel = new JLabel("x1"));

			startButton.setMinimumSize(defaultButtonSize);
			pauseButton.setMinimumSize(defaultButtonSize);
			stopButton.setMinimumSize(defaultButtonSize);
			slowerButton.setMinimumSize(defaultButtonSize);
			fasterButton.setMinimumSize(defaultButtonSize);

			startButton.setMaximumSize(defaultButtonSize);
			pauseButton.setMaximumSize(defaultButtonSize);
			stopButton.setMaximumSize(defaultButtonSize);
			slowerButton.setMaximumSize(defaultButtonSize);
			fasterButton.setMaximumSize(defaultButtonSize);
		}

		private void setNewLayout() {

			layout.setAutoCreateContainerGaps(true);

			int interGap = 10;
			int indentGap = 35;
			layout.setHorizontalGroup(layout.createSequentialGroup()
					.addGap(indentGap)
					.addGroup(layout.createParallelGroup(Alignment.CENTER)
							.addComponent(startButton)
							.addComponent(pauseButton)
							.addComponent(stopButton)
							.addComponent(slowerButton)
							.addComponent(fasterButton)
							.addComponent(currentSpeedLabel)
					)
					.addGap(indentGap)
			);

			layout.setVerticalGroup(layout.createSequentialGroup()
					.addComponent(startButton).addGap(interGap)
					.addComponent(pauseButton).addGap(interGap)
					.addComponent(stopButton).addGap(interGap)
					.addComponent(fasterButton).addGap(interGap)
					.addComponent(slowerButton).addGap(interGap)
					.addComponent(currentSpeedLabel)
			);
		}

		private void addListeners() {
			startButton.addActionListener(startListener);
			stopButton.addActionListener(stopListener);
			pauseButton.addActionListener(pauseListener);
			slowerButton.addActionListener(slowerListener);
			fasterButton.addActionListener(fasterListener);
		}


		public void refreshSpeedLabel() {
			int speed = controller.animationSpeed();
			if (speed<0) {
				currentSpeedLabel.setText("x1/"+mainPanel.getNumberFormat().format(-speed));
			} else {
				currentSpeedLabel.setText("x"+mainPanel.getNumberFormat().format(speed));				
			}
		}
	}


	ActionListener startListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				controller.startAnimation(settingsPanel.createParticleClass());
			}
			catch (NumberFormatException exc) {
				JOptionPane.showMessageDialog(mainPanel.getMainFrame()
						, bundle.getString("errorInput"),bundle.getString("error"),
						JOptionPane.ERROR_MESSAGE);
			}

			controlPanel.refreshSpeedLabel();
		}
	};

	ActionListener stopListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			controller.stopAnimation();
		}
	};

	ActionListener pauseListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			controller.pauseAnimation();
		}
	};

	public void blockSettings(boolean b) {
		settingsPanel.blockSettings(b);
	}

	ActionListener slowerListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			controller.slower();
		}
	};

	ActionListener fasterListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			controller.faster();
		}
	};

	public ControlPanel getControlPanel() {
		return controlPanel;
	}

	public SettingsPanel getSettingsPanel() {
		return settingsPanel;
	}



}