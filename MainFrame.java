package pl.edu.pw.fizyka.pojava.a3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

public class MainFrame extends JFrame {


	/**
	 * 
	 */
	private static final long       serialVersionUID        = 1L;
	private MainPanel mainPanel;
	private JMenuBar menuBar;
	private JMenu plotMenu;
	private JMenu langMenu;
	private JMenuItem polishItem, englishItem;
	private JMenu plotTopMenu, plotBottomMenu;
	private ArrayList<JRadioButtonMenuItem> topMenuItems;
	private ArrayList<JRadioButtonMenuItem> bottomMenuItems;

	private boolean isFromApplet;
	
	private ResourceBundle bundle;

	public MainFrame(Locale locale, boolean applet) throws HeadlessException {
		isFromApplet = applet;
		mainPanel = new MainPanel(this,locale);
		bundle = mainPanel.getBundle();

		this.setLayout(new GridLayout(1,1));
		if (!isFromApplet) {
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		}

		this.setTitle(bundle.getString("title"));
		this.add(mainPanel, BorderLayout.LINE_START);
		this.setSize(new Dimension(800,720));

		createMenus();

	}

	private void createMenus() {
		menuBar = new JMenuBar();
		plotMenu = new JMenu(bundle.getString("chartsMenu"));

		String[] quantityItems = new String[] {
				bundle.getString("xPos"),
				bundle.getString("yPos"),
				bundle.getString("xVel"),
				bundle.getString("yVel"),
				bundle.getString("velValue"),
				bundle.getString("energy"),
				bundle.getString("volt"),
		};

		topMenuItems = new ArrayList<JRadioButtonMenuItem>();
		bottomMenuItems = new ArrayList<JRadioButtonMenuItem>();
		plotTopMenu = new JMenu(bundle.getString("firstChart"));
		plotBottomMenu = new JMenu(bundle.getString("secondChart"));

		ButtonGroup topGroup, bottomGroup;
		topGroup = new ButtonGroup();
		bottomGroup = new ButtonGroup();

		for (String text: quantityItems) {
			JRadioButtonMenuItem topItem = new JRadioButtonMenuItem(text);
			topMenuItems.add(topItem);
			plotTopMenu.add(topItem);
			topItem.addActionListener(topMenuListener);
			topGroup.add(topItem);

			JRadioButtonMenuItem bottomItem = new JRadioButtonMenuItem(text);
			bottomMenuItems.add(bottomItem);
			plotBottomMenu.add(bottomItem);
			bottomItem.addActionListener(bottomMenuListener);
			bottomGroup.add(bottomItem);
		}
		plotMenu.add(plotTopMenu);
		plotMenu.add(plotBottomMenu);
		menuBar.add(plotMenu);

		this.setJMenuBar(menuBar);

		topMenuItems.get(4).setSelected(true);
		topMenuItems.get(4).getActionListeners();
		bottomMenuItems.get(5).setSelected(true);
		
		langMenu = new JMenu(bundle.getString("languages"));
		menuBar.add(langMenu);
		polishItem = new JMenuItem("polski");
		englishItem = new JMenuItem("English");
		langMenu.add(polishItem);
		langMenu.add(englishItem);
		
		polishItem.addActionListener(languageListener);
		englishItem.addActionListener(languageListener);
	}
	
	//Listens to language menu items
	ActionListener languageListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(polishItem)) {
				rebuildWindow(new Locale("pl"));
			} else if (e.getSource().equals(englishItem)) {
				rebuildWindow(new Locale("en"));
			}
		}
	};
	
	//builds new window with new locale (new mainPanel + new menus)
	private void rebuildWindow(Locale newLocale) {
		this.remove(mainPanel);
		mainPanel = new MainPanel(this, newLocale);
		bundle = mainPanel.getBundle();
		createMenus();
		this.add(mainPanel, BorderLayout.LINE_START);
		this.setSize(this.getWidth()+1, this.getHeight());
		this.setSize(this.getWidth()-1, this.getHeight());
		this.setTitle(bundle.getString("title"));
		repaint();
	}
	
	ActionListener topMenuListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JRadioButtonMenuItem source = (JRadioButtonMenuItem)e.getSource();
			if (source.equals(topMenuItems.get(0))) {
				mainPanel.setPlotQuantity(0, PlotPanel.QuantityType.XPOS);
			} else if (source.equals(topMenuItems.get(1))) {
				mainPanel.setPlotQuantity(0, PlotPanel.QuantityType.YPOS);
			} else if (source.equals(topMenuItems.get(2))) {
				mainPanel.setPlotQuantity(0, PlotPanel.QuantityType.VELOCITY_X);
			} else if (source.equals(topMenuItems.get(3))) {
				mainPanel.setPlotQuantity(0, PlotPanel.QuantityType.VELOCITY_Y);
			} else if (source.equals(topMenuItems.get(4))) {
				mainPanel.setPlotQuantity(0, PlotPanel.QuantityType.VELOCITY_MODULE);
			} else if (source.equals(topMenuItems.get(5))) {
				mainPanel.setPlotQuantity(0, PlotPanel.QuantityType.ENERGY);
			} else if (source.equals(topMenuItems.get(6))) {
				mainPanel.setPlotQuantity(0, PlotPanel.QuantityType.VOLTAGE);
			}
		}
	};


	ActionListener bottomMenuListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JRadioButtonMenuItem source = (JRadioButtonMenuItem)e.getSource();
			if (source.equals(bottomMenuItems.get(0))) {
				mainPanel.setPlotQuantity(1, PlotPanel.QuantityType.XPOS);
			} else if (source.equals(bottomMenuItems.get(1))) {
				mainPanel.setPlotQuantity(1, PlotPanel.QuantityType.YPOS);
			} else if (source.equals(bottomMenuItems.get(2))) {
				mainPanel.setPlotQuantity(1, PlotPanel.QuantityType.VELOCITY_X);
			} else if (source.equals(bottomMenuItems.get(3))) {
				mainPanel.setPlotQuantity(1, PlotPanel.QuantityType.VELOCITY_Y);
			} else if (source.equals(bottomMenuItems.get(4))) {
				mainPanel.setPlotQuantity(1, PlotPanel.QuantityType.VELOCITY_MODULE);
			} else if (source.equals(bottomMenuItems.get(5))) {
				mainPanel.setPlotQuantity(1, PlotPanel.QuantityType.ENERGY);
			} else if (source.equals(bottomMenuItems.get(6))) {
				mainPanel.setPlotQuantity(1, PlotPanel.QuantityType.VOLTAGE);
			}

		}
	};

	public void blockSettings(boolean b) {
		plotMenu.setEnabled(!b);
		langMenu.setEnabled(!b);
	}

	public static void main(String[] args) {
		Locale locale = Locale.getDefault();
		MainFrame frame = new MainFrame(locale,false);
		frame.setVisible(true);
		frame.setResizable(false);

	}

}