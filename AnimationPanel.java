package pl.edu.pw.fizyka.pojava.a3;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.util.Range;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
/*
 * 
 * Class representing panel on which the animation is presented.
 * Uses JChart2D as plotting library. 
 * 
 */
public class AnimationPanel extends JPanel {

	/**
	 * 
	 */
	private static final long       serialVersionUID        = 1L;

	private MainPanel mainPanel;

	private ResourceBundle bundle;
	
	private Chart2D chart;
	private ITrace2D trace;
	private ITrace2D bottom,top;
	private ITrace2D force, forceArrow;

	private JPanel infoPanel;
	
	private JCheckBox showingForceBox;
	private JLabel currentTimeLabel, turnsCounterLabel, energyLabel;
	private long frameCounter;
	private NumberFormat timeFormat;
	private NumberFormat energyFormat;

	public AnimationPanel(MainPanel parent) {
		mainPanel = parent;
		this.setLayout(new BorderLayout());
		bundle = mainPanel.getBundle();
		chart = new Chart2D();
		trace = new Trace2DSimple();
		force = new Trace2DSimple();
		forceArrow = new Trace2DSimple();
		bottom=new Trace2DSimple();
		top=new Trace2DSimple();

		chart.addTrace(trace);
		chart.addTrace(bottom);
		chart.addTrace(top);
		chart.addTrace(force);
		chart.addTrace(forceArrow);

		trace.setName("");
		force.setName("");
		forceArrow.setName("");

		force.setColor(Color.GREEN);
		forceArrow.setColor(Color.GREEN);

		drawBottomAndTop();

		chart.getAxisX().setRangePolicy(new RangePolicyFixedViewport(new Range(-0.8, 0.8)));
		chart.getAxisY().setRangePolicy(new RangePolicyFixedViewport(new Range(-0.45, 0.45)));

		this.add(chart, BorderLayout.CENTER);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		trace.setColor(Color.RED);
		
		infoPanel = new JPanel();
		infoPanel.setBackground(chart.getBackground());
		
		infoPanel.add(currentTimeLabel = new JLabel(bundle.getString("time")));
		infoPanel.add(turnsCounterLabel = new JLabel(bundle.getString("turns")));
		infoPanel.add(energyLabel = new JLabel(bundle.getString("currentEnergy")));

		frameCounter=0;

		timeFormat = NumberFormat.getInstance(Locale.ENGLISH);
		timeFormat.setMaximumFractionDigits(4);
		timeFormat.setMinimumFractionDigits(4);
		timeFormat.setGroupingUsed(false);
		
		energyFormat = NumberFormat.getInstance(Locale.ENGLISH);
		energyFormat.setMaximumFractionDigits(3);
		energyFormat.setMinimumFractionDigits(3);
		energyFormat.setGroupingUsed(false);

		showingForceBox= new JCheckBox(mainPanel.getBundle().getString("showForce"));
		showingForceBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mainPanel.getController().setShowingForce(showingForceBox.isSelected());
			}
		});
		showingForceBox.setBackground(Color.WHITE);

		infoPanel.add(showingForceBox);
		this.add(infoPanel,BorderLayout.PAGE_START);
	}

	/* 
	 * addPoint:
	 * adding point to animation, called from animation timer in "Controller" class.
	 */
	public void addPoint(Particle.Result result, boolean showForce) {
		if (frameCounter % 5 == 0) {
			currentTimeLabel.setText(bundle.getString("time")+" " +
				timeFormat.format(result.getTime()*1e6)+ "μs |");
			turnsCounterLabel.setText(bundle.getString("turns") + " " +
				String.valueOf(result.getTurns()) + " |");
			energyLabel.setText(bundle.getString("currentEnergy") + " " +
				energyFormat.format(result.getEnergy()) + "/" +
				energyFormat.format(result.getMaxEnergy())+ " MeV |");
		}
		frameCounter++;

		trace.addPoint(result.getX(),result.getY());
		force.removeAllPoints();
		forceArrow.removeAllPoints();
		if (showForce==true) {
			addForceVector(result.getFX(), result.getFY(), result.getX(), result.getY());
		}
	}

	/* 
	 * addForceVector:
	 * Drawing current force vector on animation.
	 */

	private void addForceVector(double Fx, double Fy, double X, double Y) {
		force.addPoint(X,Y);
		force.addPoint(X+Fx, Y+Fy);
		double F=Math.sqrt(Fx*Fx+Fy*Fy);
		double Nx=Fx/F, Ny=Fy/F;
		double x1,y1,x2,y2,x3,y3;
		double arrowSize = (F > 0.1 ? 0.03 : F*0.3);
		x2=X+Fx;
		y2=Y+Fy;
		x1=X+Fx-arrowSize*(Nx*Math.cos(Math.PI/6)-Ny*Math.sin(Math.PI/6));
		y1=Y+Fy-arrowSize*(Nx*Math.sin(Math.PI/6)+Ny*Math.cos(Math.PI/6));
		x3=X+Fx-arrowSize*(Nx*Math.cos(-Math.PI/6)-Ny*Math.sin(-Math.PI/6));
		y3=Y+Fy-arrowSize*(Nx*Math.sin(-Math.PI/6)+Ny*Math.cos(-Math.PI/6));

		forceArrow.addPoint(x1,y1);
		forceArrow.addPoint(x2,y2);
		forceArrow.addPoint(x3,y3);
		forceArrow.addPoint(x1,y1);

	}

	//self explanatory
	public void clear() {
		trace.removeAllPoints();
	}

	/*
	 * drawBottomAndTop:
	 * Creates top and bottom dees drawing (in seperate datasets).
	 */
	private void drawBottomAndTop() {

		bottom.setName("");
		top.setName("");
		top.setStroke(new BasicStroke(1,BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
				10.0f, new float[] {4f,4f}, 0f));
		bottom.setStroke(new BasicStroke(1,BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
				10.0f, new float[] {4f,4f}, 0f));
		top.setColor(Color.GRAY);
		bottom.setColor(Color.GRAY);
		double R=Particle.radius, gap=Particle.gap;

		top.addPoint(Math.sqrt(R*R-gap*gap),gap);
		for (double t=0; t<=Math.PI; t+=Math.PI/100) {
			double x=0.4*Math.cos(t), y=0.4*Math.sin(t);
			if (y>=gap || y<=-gap) {
				top.addPoint(x,y);
			}
		}
		top.addPoint(-Math.sqrt(R*R-gap*gap),gap);
		top.addPoint(Math.sqrt(R*R-gap*gap),gap);

		bottom.addPoint(-Math.sqrt(R*R-gap*gap),-gap);
		for (double t=Math.PI; t<=2*Math.PI; t+=Math.PI/100) {
			double x=0.4*Math.cos(t), y=0.4*Math.sin(t);
			if (y>=gap || y<=-gap) {
				bottom.addPoint(x,y);
			}
		}
		bottom.addPoint(Math.sqrt(R*R-gap*gap),-gap);
		bottom.addPoint(-Math.sqrt(R*R-gap*gap),-gap);
	}
}