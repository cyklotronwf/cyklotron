package pl.edu.pw.fizyka.pojava.a3;

/*
 * Physical:
 * Physical constants used in app, stored here for tidiness.
 */
public abstract class Physical {

	public static final double ELEMENTAR_CHARGE=1.602e-19;
	public static final double MASS_UNIT = 1.661e-27;
	public static final double ELECTRON_MASS = 9.109e-31;
	public final static double LIGHT_SPEED = 299792458;


}
