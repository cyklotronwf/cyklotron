package pl.edu.pw.fizyka.pojava.a3;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.util.Locale;

import javax.swing.JApplet;
import javax.swing.JLabel;

import pl.edu.pw.fizyka.pojava.a3.MainFrame;


/*
 * AppletClass:
 * JApplet class
 */
public class AppletClass extends JApplet {

	private static final long	serialVersionUID	= 1L;

	public AppletClass() throws HeadlessException {
		this.setLayout(new FlowLayout());
		this.add(new JLabel("<html>Powinno pojawić się okno<br>"+
				"There should be a window</html>"));
		Locale locale = Locale.getDefault();
		MainFrame frame = new MainFrame(locale, true);
		frame.setVisible(true);
		frame.setResizable(false);
	}
	
}
