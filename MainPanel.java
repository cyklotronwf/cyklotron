package pl.edu.pw.fizyka.pojava.a3;

import java.awt.Color;
import java.awt.Dimension;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.Timer;


/*
 * MainPanel:
 * Main class, holds all subclasses and builds the interface.
 */
public class MainPanel extends JPanel {

	/**
	 * 
	 */
	private static final long       serialVersionUID        = 1L;
	private LeftPanel leftPanel;
	private AnimationPanel animationPanel;
	private PlotPanel plotBottomPanel;
	private PlotPanel plotTopPanel;
	private Controller controller;
	private MainFrame mainFrame;
	private NumberFormat numberFormat;
	private Timer animationTimer=null;
	private ResourceBundle bundle;

	public MainPanel(MainFrame frame, Locale locale) {
		
		if (locale==null) {
			locale = Locale.getDefault();
		}
		bundle = ResourceBundle.getBundle("LabelBundle",locale);
		mainFrame = frame;
		controller = new Controller(this);

		numberFormat = NumberFormat.getInstance(Locale.ENGLISH);
		numberFormat.setGroupingUsed(false);
		numberFormat.setMaximumFractionDigits(2);

		this.add(leftPanel = new LeftPanel(this));

		this.add(animationPanel = new AnimationPanel(this));
		animationPanel.setPreferredSize(new Dimension(500,250));

		this.add(plotTopPanel = new PlotPanel(this,PlotPanel.QuantityType.VELOCITY_MODULE,Color.GREEN));
		plotTopPanel.setPreferredSize(new Dimension(660,22));

		this.add(plotBottomPanel = new PlotPanel(this,PlotPanel.QuantityType.ENERGY,Color.BLUE));
		plotBottomPanel.setPreferredSize(new Dimension(plotTopPanel.getSize()));

		GroupLayout layoutMain = new GroupLayout(this);
		this.setLayout(layoutMain);
		layoutMain.setAutoCreateContainerGaps(true);
		layoutMain.setAutoCreateGaps(true);

		layoutMain.setHorizontalGroup(layoutMain.createSequentialGroup()
				.addComponent(leftPanel)
				.addGroup(layoutMain.createParallelGroup()
						.addGroup(layoutMain.createParallelGroup()
								.addComponent(animationPanel)
								.addComponent(plotTopPanel)
								.addComponent(plotBottomPanel)
						)
				)
		);

		layoutMain.setVerticalGroup(layoutMain.createParallelGroup()
				.addComponent(leftPanel)
				.addGroup(layoutMain.createSequentialGroup()
						.addGroup(layoutMain.createSequentialGroup()
								.addComponent(animationPanel)
								.addComponent(plotTopPanel)
								.addComponent(plotBottomPanel)
						)
				)
		);      
	}

	public NumberFormat getNumberFormat() {
		return numberFormat;
	}

	/*
	 * Blocks the settings; called from starting methon in Controller, calls
	 * subclasses' methods.
	 */

	 public void blockSettings(boolean b) {
		 leftPanel.blockSettings(b);
		 mainFrame.blockSettings(b);
	 }

	 /*
	  * Clears all data in plots for new animation.
	  */
	  public void resetPlots() { 

		  animationPanel.clear();
		  plotTopPanel.newSettings();
		  plotBottomPanel.newSettings();
	  }

	 /*
	  * Sets which quantity (velocity, energy ...) is displayed on chosen
	  * (0, 1 - top, bottom) plot panel.
	  */
	  public void setPlotQuantity(int plotPanelNr, PlotPanel.QuantityType qt) {
		 if (!controller.isRunning()) {
			 if (plotPanelNr == 0) {
				 plotTopPanel.setQuantityType(qt);
			 } else if (plotPanelNr == 1) {
				 plotBottomPanel.setQuantityType(qt);
			 }
		 }
	 }

	 // Refreshes speed label, called from method changing animation speed.
	 public void refreshSpeedLabel() {
		 leftPanel.getControlPanel().refreshSpeedLabel();
	 }

	 // Tells if force on animation plot is being showed.
	 public boolean isShowingForce() {
		 return leftPanel.getControlPanel().showForceBox.isSelected();
	 }

	public AnimationPanel getAnimationPanel() {
		return animationPanel;
	}

	public PlotPanel getPlotBottomPanel() {
		return plotBottomPanel;
	}

	public PlotPanel getPlotTopPanel() {
		return plotTopPanel;
	}

	public Controller getController() {
		return controller;
	}

	public MainFrame getMainFrame() {
		return mainFrame;
	}

	public Timer getAnimationTimer() {
		return animationTimer;
	}

	public ResourceBundle getBundle() {
		return bundle;
	}
	 
}
