package pl.edu.pw.fizyka.pojava.a3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

/*
 * Class controlling the animation start, stop, speed.
 */

public class Controller {

	private MainPanel mainPanel;
	private Particle particle;

	private Timer animationTimer;
	private boolean showingForce;

	private int speed;
	private int animationCounter;

	private static final int animationDelayTime = 20;

	public Controller(MainPanel parent) {
		mainPanel = parent;
		showingForce=false;
	}

	/*
	 * startAnimation:
	 * Called from startButton from ControlPanel.
	 * Creates a timer and computation class.
	 */

	public void startAnimation(Particle p) {
		if (isPausedAnimation()) 
			pauseAnimation();
		else if (!isRunning()) {
			particle = p;
			speed=1;
			animationCounter=0;
			mainPanel.resetPlots();
			mainPanel.blockSettings(true);
			p.execute();
			animationTimer = new Timer(animationDelayTime, animationTimerListener);
			animationTimer.start();
		}
	}

	public void stopAnimation() {
		if (particle!=null) {
			particle.stop();
			animationTimer.stop();
			particle=null;
			animationTimer=null;
			mainPanel.blockSettings(false);
		}
	}

	public void pauseAnimation() {
		if (particle!=null && particle.isRunning()) {
			if (isPausedAnimation()==false) {
				animationTimer.stop();
			}
			else {
				animationTimer.start();
			}       
		}
	}

	public boolean isPausedAnimation() {
		if (particle==null)
			return false;
		else return !animationTimer.isRunning();
	}

	public boolean isRunning() {
		if (particle==null || particle.isRunning()==false) {
			return false;
		}
		else {
			return true;
		}
	}

	public void slower() {
		if (isRunning()) {
			//			particle.slower();
			animationCounter=0;
			if (speed>20) {
				speed-=10;
			} else if (speed>10){
				speed-=2;
			} else if (speed>1) {
				speed--;
			} else if (speed==1) {
				speed=-2;
			} else if (speed<=-2) {
				speed--;
			}
			mainPanel.refreshSpeedLabel();
		}
	}

	public void faster() {
		if (isRunning()) {
			animationCounter=0;
			if (speed>=20) {
				speed+=10;
			} else if (speed>=10){
				speed+=2;
			} else if (speed>=1) {
				speed++;
			} else if (speed==-2) {
				speed=1;
			} else if (speed<-2) {
				speed++;
			}
			mainPanel.refreshSpeedLabel();
		}
	}

	//Showing animation speed
	public int animationSpeed() {
		if (particle!=null) {
			return speed;
		}
		else
			return 0;
	}

	//Sets if force is shown on animation
	public void setShowingForce(boolean s) {
		showingForce=s;
	}

	/*
	 * animationTimerListener:
	 * Main animation listener.
	 * If speed > 0 then it adds `n` points to animation and plots,
	 * where n=speed variable;
	 * else (speed<=-1) it adds one point in one every `n`th iteration
	 * (n=-speed) 
	 */

	ActionListener animationTimerListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {

			if (particle.isRunning()==false && particle.getQueueSize()==0) {
				stopAnimation();
			} else {
				int frames=0;
				if (speed>0) {
					frames = speed;
				} else {
					if (animationCounter >= (-speed-1)) {
						animationCounter=0;
						frames=1;
					}
					else {
						animationCounter++;
					}
				}

				for (int ii=0; ii<frames; ii++) {
					if (particle.getQueueSize()>0) {
						Particle.Result result = particle.getResult();
						mainPanel.getAnimationPanel().addPoint(result,showingForce);
						mainPanel.getPlotTopPanel().addPoint(result);
						mainPanel.getPlotBottomPanel().addPoint(result);
					}
				}
			}
		}
	};


}